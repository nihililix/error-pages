{
    "extends": ["eslint:recommended"],
    "env": {
        "browser": true,
        "amd": true,
        "es6": true,
        "jquery": true
    },
    "plugins": [
        "babel"
    ],
    "parser": "babel-eslint",
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "modules": true
        }
    },
    "rules": {
        "no-console": 0,
        "no-unused-vars": ["error", { "argsIgnorePattern": "^_" }],
        "no-undef": 0,

        // Errors
        "comma-dangle": [2, "never"],
        "valid-typeof": 2,
        "no-extra-semi": 2,

        // Best Practices
        "curly": 2,
        "semi-spacing": [2, {"before": false, "after": true}],
        "semi": [2, "always"],
        "no-eq-null": 2,
        "no-eval": 2,
        "consistent-return": 2,
        "eqeqeq": 2,
        "no-multi-spaces": [2, {"exceptions": {"VariableDeclarator": true, "Property": true, "AssignmentExpression": true}}],
        "no-floating-decimal": 2,
        "no-proto": 2,
        "no-useless-call": 2,
        "no-useless-concat": 2,
        "no-void": 2,

        // Stylistic rules
        "array-bracket-spacing": [2, "never"], // var arr = [ 'foo', 'bar' ]; -> var arr = ['foo', 'bar', 'baz'];
        "brace-style": [2, "1tbs", {"allowSingleLine": false}], // } else {
        "camelcase": [2, {"properties": "always"}],
        "comma-spacing": [2, {"before": false, "after": true}], // [1 , 2] -> [1, 2]
        "comma-style": [2, "last"], // ["apples"\n, "oranges"] -> ["apples",\n"oranges"]
        "computed-property-spacing": [2, "never"], // obj[foo ] / obj[ 'foo'] -> obj[foo] / obj['foo']
        "eol-last": 2, // Require file to end with single newline
        "object-curly-spacing": ["error", "never"]
    }
}
