const path = require('path');

const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin'); //installed via npm
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
var HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

const buildPath = path.resolve(__dirname, '../');

module.exports = {
    devtool: 'source-map',
    entry: './src/index.js',
    output: {
        filename: '[name].[hash:20].js',
        path: buildPath
    },
    node: {
        fs: 'empty'
    },
    module: {
        rules: [
            {
                test:   /\.twig/,
                loader: 'twig-loader'
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',

                options: {
                    presets: ['env']
                }
            },
            {
                test: /\.(scss|css|sass)$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            // translates CSS into CommonJS
                            loader: 'css-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            // Runs compiled CSS through postcss for vendor prefixing
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            // compiles Sass to CSS
                            loader: 'sass-loader',
                            options: {
                                outputStyle: 'expanded',
                                sourceMap: true,
                                sourceMapContents: true
                            }
                        }
                    ],
                    fallback: 'style-loader'
                }),
            },
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: '[name].[hash:20].[ext]',
                            limit: 8192
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'bad_gateway.html',
            template: './src/templates/bad_gateway.twig',
            inlineSource: './styles.[md5:contenthash:hex:20].(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'bad_request.html',
            template: './src/templates/bad_request.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: './styles.[md5:contenthash:hex:20].(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'forbidden.html',
            template: './src/templates/forbidden.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/templates/index.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'internal_server_error.html',
            template: './src/templates/internal_server_error.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'maintenance.html',
            template: './src/templates/maintenance.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'method_not_allowed.html',
            template: './src/templates/method_not_allowed.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
        }),
        new HtmlWebpackPlugin({
            filename: 'not_acceptable.html',
            template: './src/templates/not_acceptable.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'not_found.html',
            template: './src/templates/not_found.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'not_implemented.html',
            template: './src/templates/not_implemented.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'precondition_failed.html',
            template: './src/templates/precondition_failed.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'proxy_authentication_required.html',
            template: './src/templates/proxy_authentication_required.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'request-uri_too_long.html',
            template: './src/templates/request-uri_too_long.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
        }),
        new HtmlWebpackPlugin({
            filename: 'unauthorized.html',
            template: './src/templates/unauthorized.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackPlugin({
            filename: 'unsupported_media_type.html',
            template: './src/templates/unsupported_media_type.twig',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
            inlineSource: '.(js|css)$'
        }),
        new HtmlWebpackInlineSourcePlugin(),
        // new CleanWebpackPlugin(buildPath),
        new FaviconsWebpackPlugin({
            // Your source logo
            logo: './src/assets/icon.png',
            // The prefix for all image files (might be a folder or a name)
            prefix: 'icons-[hash]/',
            // Generate a cache file with control hashes and
            // don't rebuild the favicons until those hashes change
            persistentCache: true,
            // Inject the html into the html-webpack-plugin
            inject: true,
            // favicon background color (see https://github.com/haydenbleasel/favicons#usage)
            background: '#fff',
            // favicon app title (see https://github.com/haydenbleasel/favicons#usage)
            title: '{{projectName}}',

            // which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
            icons: {
                android: true,
                appleIcon: true,
                appleStartup: true,
                coast: false,
                favicons: true,
                firefox: true,
                opengraph: false,
                twitter: false,
                yandex: false,
                windows: false
            }
        }),
        new ExtractTextPlugin('./styles.[md5:contenthash:hex:20].css', {
            allChunks: true
        }),
        new OptimizeCssAssetsPlugin({
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {
                map: {
                    inline: false,
                },
                discardComments: {
                    removeAll: true
                }
            },
            canPrint: true
        })
    ]
};
